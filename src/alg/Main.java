package alg;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

@SuppressWarnings("Duplicates")
public class Main {

    static StringTokenizer st;
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


    static int N, M;
    static int[][] costs;
    static boolean[][] pegs;
    static int bestSolution = Integer.MAX_VALUE;
    static Deque<Move> solution = new ArrayDeque<>();
    static int[] values;

    static class Move {
        int fromX, fromY, toX, toY;

        public Move(int fromX, int fromY, int toX, int toY) {
            this.fromX = fromX;
            this.fromY = fromY;
            this.toX = toX;
            this.toY = toY;
        }
    }

    static enum Direction {
        //        UP(new int[]{-1, 0}),
//        LEFT(new int[]{0, -1}),
//        RIGHT(new int[]{0, 1}),
//        DOWN(new int[]{1, 0});
        DOWN(new int[]{1, 0}),
        RIGHT(new int[]{0, 1}),
        LEFT(new int[]{0, -1}),
        UP(new int[]{-1, 0});


        int[] vector;

        Direction(int[] vector) {
            this.vector = vector;
        }
    }

    static boolean validCoords(int x, int y) {
        return x >= 0 && y >= 0 && x < M && y < N;
    }

    static boolean validMove(int x, int y, Direction dir) {
        if (!pegs[x][y]) return false;
        int X = x + dir.vector[0];
        int Y = y + dir.vector[1];
        if (!validCoords(X, Y) || !pegs[X][Y]) return false;
        X = X + dir.vector[0];
        Y = Y + dir.vector[1];
        return validCoords(X, Y) && !pegs[X][Y];
    }

    static boolean validMovex(int x, int y, Direction dir) {
        if (pegs[x][y]) return false;
        int X = x + dir.vector[0];
        int Y = y + dir.vector[1];
        if (!validCoords(X, Y) || !pegs[X][Y]) return false;
        X = X + dir.vector[0];
        Y = Y + dir.vector[1];
        return validCoords(X, Y) && pegs[X][Y];
    }

    static boolean doMove(int x, int y, Direction dir) {
        pegs[x][y] = true;
        int X = x + dir.vector[0];
        int Y = y + dir.vector[1];
        pegs[X][Y] = false;
        X = X + dir.vector[0];
        Y = Y + dir.vector[1];
        pegs[X][Y] = false;
        X = X - 3 * dir.vector[0];
        Y = Y - 3 * dir.vector[1];
        if (validCoords(X, Y) && pegs[X][Y]) {
            pegs[X][Y] = false;
            return true;
        }
        return false;
    }

    static void undoMove(int x, int y, boolean third, Direction dir) {
        pegs[x][y] = true;
        int X = x - dir.vector[0];
        int Y = y - dir.vector[1];
        pegs[X][Y] = true;
        X = X - dir.vector[0];
        Y = Y - dir.vector[1];
        pegs[X][Y] = false;
        X = X - dir.vector[0];
        Y = Y - dir.vector[1];
        if (validCoords(X, Y) && third) pegs[X][Y] = true;
    }

    static int solve(int scoreAcc, Deque<Move> moves) {
        boolean finalState = true;
        if (scoreAcc >= bestSolution) {
            return scoreAcc;
        }

        int best = Integer.MAX_VALUE;
        int X, Y;
        boolean third;
        for (int popPeg : values) {

            for (int x = 0; x < M; x++) {
                for (int y = 0; y < N; y++) {
                    if (costs[x][y] == popPeg) {
                        for (Direction dir : Direction.values()) {
                            if (validMovex(x, y, dir)) {
                                finalState = false;

//                                System.out.println("from");
//                                printMatrix();
//                                printOutput();
                                X = x + 2 * dir.vector[0];
                                Y = y + 2 * dir.vector[1];
                                scoreAcc += costs[x][y];
                                moves.addLast(new Move(X, Y, x, y));
                                third = doMove(x, y, dir);
                                best = Math.min(solve(scoreAcc, moves), best);

//                                System.out.println("to");
//                                printMatrix();
//                                printOutput();

                                undoMove(X, Y, third, dir);
                                moves.removeLast();
                                scoreAcc -= costs[x][y];
                            }
                        }
                    }

                }
            }
        }

        if (finalState) {
            bestSolution = scoreAcc;
            solution.clear();
            solution.addAll(moves);
        //    System.out.println(scoreAcc);
            return scoreAcc;
        }

        return scoreAcc + best;
    }

    private static boolean makeMove() {
        return false;
    }


    static void parseInput(List<String> lines) {
        int point = 0;
        values = new int[N * M];

        String token;
        for (int i = 0; i < M; i++) {
            st = new StringTokenizer(lines.get(i));
            for (int j = 0; j < N; j++) {
                token = st.nextToken();
                if (token.charAt(0) == '#') {
                    pegs[i][j] = true;
                    costs[i][j] = Integer.parseInt(token.substring(1));
                    values[point] = Integer.parseInt(token.substring(1));
                    point++;
                } else {
                    pegs[i][j] = false; // redundant
                    costs[i][j] = Integer.parseInt(token);
                    values[point] = (Integer.parseInt(token));
                    point++;
                }
            }
        }
    }

    static void readInput() throws IOException {
        List<String> lines = new LinkedList<>();
        String line;
        while ((line = br.readLine()) != null && !line.equals("")) {
            lines.add(line);
        }
        M = lines.size();
        st = new StringTokenizer(lines.get(0));
        N = st.countTokens();
        costs = new int[M][N];
        pegs = new boolean[M][N];
        parseInput(lines);
    }

    public static void main(String[] args) throws IOException {
          long t1 = System.currentTimeMillis();
//          br = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\Users\\juras\\IdeaProjects\\alg1.1\\src\\pub03.in")));
        readInput();
        Arrays.sort(values);
        solve(0, new ArrayDeque<>());
        printOutput();
        System.out.println();
           System.out.println(System.currentTimeMillis() - t1);
        // System.out.printf("%d\n", bestSolution);
    }

    private static void printMatrix() {
        for (int i = 0; i < costs.length; i++) {
            if (costs[i][0] == 0) {
                break;
            }
            for (int j = 0; j < costs[i].length; j++) {
                if (costs[i][j] == 0) {
                    break;
                }
                System.out.print(pegs[i][j] ? 1 + " " : 0 + " ");
            }
            System.out.println();
        }
        System.out.println("--------------------");
    }

    private static void printOutput() {
        if (solution.isEmpty()) return;
        System.out.printf("%d\n", bestSolution);
        Move first = solution.removeFirst();
        System.out.printf("[%d,%d]->[%d,%d]", first.fromX, first.fromY, first.toX, first.toY);
        for (Move m : solution) {
            System.out.printf("  [%d,%d]->[%d,%d]", m.fromX, m.fromY, m.toX, m.toY);
        }
    }

}
