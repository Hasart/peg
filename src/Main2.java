import java.io.*;
import java.util.*;

public class Main2 {

    static StringTokenizer st;
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    static int N, M;
    static int[][] costs;
    static boolean[][] pegs;
    static int bestSolution;
    static Deque<Move> solution = new ArrayDeque<>();


    static class Move {
        int fromX, fromY, toX, toY;

        public Move(int fromX, int fromY, int toX, int toY) {
            this.fromX = fromX;
            this.fromY = fromY;
            this.toX = toX;
            this.toY = toY;
        }
    }

    static enum Direction {
        UP(new int[]{-1, 0}),
        RIGHT(new int[]{0, 1}),
        DOWN(new int[]{1, 0}),
        LEFT(new int[]{0, -1});

        int[] vector;

        Direction(int[] vector) {
            this.vector = vector;
        }
    }

    static boolean validCoords(int x, int y) {
        return x >= 0 && y >= 0 && x < M && y < N;
    }

    static boolean validMove(int x, int y, Direction dir) {
        if (!pegs[x][y]) return false;
        int X = x + dir.vector[0];
        int Y = y + dir.vector[1];
        if (!validCoords(X, Y) || !pegs[X][Y]) return false;
        X = X + dir.vector[0];
        Y = Y + dir.vector[1];
        return validCoords(X, Y) && !pegs[X][Y];
    }

    static boolean doMove(int x, int y, Direction dir) {
        pegs[x][y] = false;
        int X = x + dir.vector[0];
        int Y = y + dir.vector[1];
        pegs[X][Y] = false;
        X = X + dir.vector[0];
        Y = Y + dir.vector[1];
        pegs[X][Y] = true;
        X = X + dir.vector[0];
        Y = Y + dir.vector[1];
        if (validCoords(X, Y) && pegs[X][Y]) {
            pegs[X][Y] = false;
            return true;
        }
        return false;
    }

    static void undoMove(int x, int y, boolean third, Direction dir) {
        pegs[x][y] = true;
        int X = x + dir.vector[0];
        int Y = y + dir.vector[1];
        pegs[X][Y] = true;
        X = X + dir.vector[0];
        Y = Y + dir.vector[1];
        pegs[X][Y] = false;
        X = X + dir.vector[0];
        Y = Y + dir.vector[1];
        if (validCoords(X, Y) && third) pegs[X][Y] = true;
    }

    static int solve(int scoreAcc, Deque<Move> moves) {
        boolean finalState = true;
        if (scoreAcc >= bestSolution) return scoreAcc;

        int best = Integer.MAX_VALUE;
        int X, Y;
        boolean third;
        for (int x = 0; x < M; x++) {
            for (int y = 0; y < N; y++) {
                for (Direction dir : Direction.values()) {
                    if (validMove(x, y, dir)) {
                        finalState = false;
                        if (costs[x][y] + scoreAcc <= bestSolution) {
                            X = x + 2 * dir.vector[0];
                            Y = y + 2 * dir.vector[1];
                            scoreAcc += costs[X][Y];
                            moves.addLast(new Move(x, y, X, Y));
                            third = doMove(x, y, dir);
                            best = Math.min(solve(scoreAcc, moves), best);
                            undoMove(x, y, third, dir);
                            moves.removeLast();
                            scoreAcc -= costs[X][Y];
                        }
                    }
                }
            }
        }

        if (finalState) {
            bestSolution = scoreAcc;
            solution.clear();
            solution.addAll(moves);
            return scoreAcc;
        }
        return scoreAcc + best;
    }

    static void parseInput(List<String> lines) {
        String token;
        for (int i = 0; i < M; i++) {
            st = new StringTokenizer(lines.get(i));
            for (int j = 0; j < N; j++) {
                token = st.nextToken();
                if (token.charAt(0) == '#') {
                    pegs[i][j] = true;
                    costs[i][j] = Integer.parseInt(token.substring(1));
                } else {
                    pegs[i][j] = false; // redundant
                    costs[i][j] = Integer.parseInt(token);
                }
            }
        }

        int max, x1, y1;
        for (int x = 0; x < M; x++) {
            for (int y = 0; y < N; y++) {
                if (pegs[x][y]) {
                    max = Integer.MIN_VALUE;
                    for (Direction dir : Direction.values()) {
                        x1 = x + dir.vector[0];
                        y1 = y + dir.vector[1];
                        if (validCoords(x1, y1)) {
                            max = Math.max(max, costs[x1][y1]);
                        }
                    }
                    bestSolution += max;
                }
            }
        }
    }

    static void readInput() throws IOException {
        List<String> lines = new LinkedList<>();
        String line;
        while ((line = br.readLine()) != null && !line.equals("")) {
            lines.add(line);
        }
        M = lines.size();
        st = new StringTokenizer(lines.get(0));
        N = st.countTokens();
        costs = new int[M][N];
        pegs = new boolean[M][N];
        parseInput(lines);
    }

    public static void main(String[] args) throws IOException {
        long t1 = System.currentTimeMillis();
        br = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\Users\\juras\\IdeaProjects\\alg1.1\\src\\pub01.in")));

        readInput();
        solve(0, new ArrayDeque<>());
        printOutput();

        System.out.println();
        System.out.println(System.currentTimeMillis() - t1);
    }

    private static void printOutput() {
        System.out.printf("%d\n", bestSolution);
        Move first = solution.removeFirst();
        System.out.printf("[%d,%d]->[%d,%d]", first.fromX, first.fromY, first.toX, first.toY);
        for (Move m : solution) {
            System.out.printf("  [%d,%d]->[%d,%d]", m.fromX, m.fromY, m.toX, m.toY);
        }
    }
}
