import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

@SuppressWarnings("Duplicates")
public class Mainm {
    private static int[][] matrix; //x y
    private static boolean[][] pegs;
    private static ArrayList<Integer> values;
    private static ArrayList<Integer> path = new ArrayList<>();
    // private static SortedSet<Integer> values ;
    private static int leastTotalCost = Integer.MAX_VALUE;
    private static ArrayList<Integer> bestPath;
    private static int tempCost = 0;

    private static void fillMatrixes() {
        matrix = new int[10][30];
        pegs = new boolean[10][30];
        values = new ArrayList<>();
        //values = new TreeSet<>();
        BufferedReader br;
        int znak;


        // BufferedReader br = new BufferedReader(new InputStreamReader(System.in));  // http://docs.oracle.com/javase/7/docs/api/java/io/InputStreamReader.html
        try {
            br = new BufferedReader(new FileReader("C:\\Users\\juras\\IdeaProjects\\alg1.1\\src\\pub01.in"));// just for testing
            int x = 0;
            int y = 0;
            boolean temp = false; // boolean  1=true
            do {
                znak = br.read();
                if (znak == 32) {
                    continue;
                } // znak je mezera
                else if (znak == 13 || znak == -1) {
                    continue;
                } // znak je \n novy radek
                else if (znak == 10) {
                    x++;
                    y = 0;
                    continue;
                } // znak je \n novy radek
                else if (znak == 35) {
                    temp = true;
                    continue;
                } // znak je #
                else {
                    znak = Character.getNumericValue(znak);
                    matrix[x][y] = znak;
                    values.add(znak);
                    if (temp) {
                        pegs[x][y] = true;
                    }
                }
                y++;
                temp = false;
            } while (znak != -1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void findCorrectMove(int i, int k, int popPeg) {

        if (i - 2 >= 0 && pegs[i - 2][k] && pegs[i - 1][k] && !pegs[i][k]) { // zezhora
            printMatrix();
            pegs[i - 2][k] = false;
            pegs[i - 1][k] = false;
            pegs[i][k] = true;

            tempCost += popPeg;
            path.add(i - 2);
            path.add(k);
            path.add(i);
            path.add(k);
            if (pegs[i + 1][k]) {
                pegs[i + 1][k] = false;
                path.add(1);
            } else {
                path.add(0);
            }
            System.out.println("popPeg: " + popPeg + "    cost: " + tempCost);
            printPath(path);
            System.out.println();
        }
        if (k - 2 >= 0 && pegs[i][k - 2] && pegs[i][k - 1] && !pegs[i][k]) { //zprava
            printMatrix();
            pegs[i][k - 2] = false;
            pegs[i][k - 1] = false;
            pegs[i][k] = true;

            tempCost += popPeg;
            path.add(i);
            path.add(k - 2);
            path.add(i);
            path.add(k);
            if (pegs[i][k + 1]) {
                pegs[i][k + 1] = false;
                path.add(1);
            } else {
                path.add(0);
            }
            System.out.println("popPeg: " + popPeg + "    cost: " + tempCost);
            printPath(path);
            System.out.println();
        }
        if (pegs[i + 2][k] && pegs[i + 1][k] && !pegs[i][k]) { //zespoda
            printMatrix();
            pegs[i + 2][k] = false;
            pegs[i + 1][k] = false;
            pegs[i][k] = true;

            tempCost += popPeg;
            path.add(i + 2);
            path.add(k);
            path.add(i);
            path.add(k);
            if (pegs[i - 1][k]) {
                pegs[i - 1][k] = false;
                path.add(1);
            } else {
                path.add(0);
            }
            System.out.println("popPeg: " + popPeg + "    cost: " + tempCost);
            printPath(path);
            System.out.println();

        }
        if (pegs[i][k + 2] && pegs[i][k + 1] && !pegs[i][k]) {// zleva
            printMatrix();
            pegs[i][k + 2] = false;
            pegs[i][k + 1] = false;
            pegs[i][k] = true;

            tempCost += popPeg;
            path.add(i);
            path.add(k + 2);
            path.add(i);
            path.add(k);
            if (pegs[i][k - 1]) {
                pegs[i][k - 1] = false;
                path.add(1);
            } else {
                path.add(0);
            }
            System.out.println("popPeg: " + popPeg + "    cost: " + tempCost);
            printPath(path);
            System.out.println();

        }
    }

    /**
     * najde reseni, ale nemusi byt cely, a proto je ve do while cyklu
     *
     * @return -1 pikud ta cesta je horsi,nez byvala,         jinak vrati cenu
     */
    private static int makeMoves() {
        int popPeg = 1000;
        for (int a : values) {
            popPeg = a;

            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i][0] == 0) {
                    break;
                }
                for (int j = 0; j < matrix[i].length; j++) {
                    if (matrix[i][j] == 0) {
                        break;
                    }
                    // System.out.print( pegs[i][j] ? 1+" " : 0 +" ");
                    if (matrix[i][j] == popPeg) {
                        findCorrectMove(i, j, popPeg);
                        if (tempCost > leastTotalCost) {
                            return -1;
                        }
                    }
                }
                //System.out.println();
            }
        }
        return popPeg;
    }

    private static boolean isThereValidMove() {
        boolean btemp = false;
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i][0] == 0) {
                break;
            }
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == 0) {
                    break;
                }

                if (i - 2 >= 0 && pegs[i - 2][j] && pegs[i - 1][j] && !pegs[i][j]) { // zezhora
                    btemp = true;
                }
                if (j - 2 >= 0 && pegs[i][j - 2] && pegs[i][j - 1] && !pegs[i][j]) { //zprava
                    btemp = true;

                }
                if (pegs[i + 2][j] && pegs[i + 1][j] && !pegs[i][j]) { //zespoda
                    btemp = true;


                }
                if (pegs[i][j + 2] && pegs[i][j + 1] && !pegs[i][j]) {// zleva
                    btemp = true;

                }

            }
        }
        return btemp;
    }

    private static void backTrack() {
        int a, b, c, d, e;
        int len = path.size() - 1;
        e = path.remove(len);
        d = path.remove(len - 1);
        c = path.remove(len - 2);
        b = path.remove(len - 3);
        a = path.remove(len - 4);
        System.out.println("temp cost: " + tempCost + "\n");
        tempCost -= matrix[c][d];
        pegs[a][b] = true;
        pegs[c][d] = false;
        pegs[(a + c) / 2][(b + d) / 2] = true;
        if (e == 1) {
            pegs[(2 * c) - ((a + c) / 2)][(2 * d) - ((b + d) / 2)] = true;

        }
    }

    public static void recursion() {  /* todo */

        do {
            if (makeMoves() == -1) {
                break;
            }
        } while (isThereValidMove());
        if (tempCost < leastTotalCost) {
            leastTotalCost = tempCost;
            bestPath = (ArrayList<Integer>) path.clone();
        }


        printMatrix();
        System.out.println();
        System.out.println("BACKTRACK");
        backTrack();
        printPath(path);
        if (path.size() >= 0) {
            recursion();
        }
    }

    public static void main(String[] args) {
        fillMatrixes();
        values.sort(Integer::compareTo);
        recursion();

        printMatrix();
        System.out.println();
        System.out.println("the least cost: " + leastTotalCost);
        printPath(bestPath);


    }

    private static void printMatrix() {
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i][0] == 0) {
                break;
            }
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == 0) {
                    break;
                }
                System.out.print(pegs[i][j] ? 1 + " " : 0 + " ");
            }
            System.out.println();
        }
        System.out.println("--------------------");
    }

    private static void printPath(ArrayList pathx) {
        for (int i = 0; i < pathx.size() / 5; i++) {
            System.out.printf("[%d,%d]->[%d,%d]  ", pathx.get(5 * i + 0), pathx.get(5 * i + 1), pathx.get(5 * i + 2), pathx.get(5 * i + 3));
        }
        System.out.println();
    }
}
